import React from 'react';
import Jobs from './components/jobs';
import Button from './components/button';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jobs: []
    };
    this.addJob = this.addJob.bind(this);
    this.removeJob = this.removeJob.bind(this);
    this.save = this.save.bind(this);
    this.onJobEntryChange = this.onJobEntryChange.bind(this);
  }

  addJob() {
    const newJobs = this.state.jobs;
    const nextId = newJobs.length + 1;
    newJobs.push(
      {
        id: nextId,
        company: "",
        title: "",
        dateApplied: ""
      }
    );
    this.setState(newJobs)
  }

  removeJob() {
    const newJobs = this.state.jobs;
    const deletedJob = newJobs.pop();
    const url = "http://localhost:8080/jobs/" + deletedJob.id;
    const requestOptions = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(deletedJob)
    }
    fetch(url, requestOptions)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        this.setState(newJobs)
    });
  }

  save() {
    const newJobs = this.state.jobs;
    const url = "http://localhost:8080/jobs";
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(newJobs)
    }
    fetch(url, requestOptions)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        this.setState({jobs: data})
    });
  }

  onJobEntryChange() {
    console.log("testing onJobEntryChange");
  }

  componentDidMount() {
    const url = "http://localhost:8080/jobs";
    fetch(url)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        this.setState({jobs: data})
    });
  }

  render() {
    return (
      <div>
        <Jobs jobs={this.state.jobs} onJobEntryChange={this.onJobEntryChange}/>
        <Button text="New job" handleClick={this.addJob} />
        <Button text="Remove job" handleClick={this.removeJob} />
        <Button text="Save" handleClick={this.save} />
      </div>
    );
  }
}
export default App;