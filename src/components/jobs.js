import React from 'react';
import './jobs.css';

class Jobs extends React.Component {
  render() {
    const jobs = this.props.jobs;
    return (
      <div>
        <h1>Job Log</h1>
        {jobs.map(job => (
          <div className="jobEntry" onChange={this.props.onJobEntryChange} key={job.id}>
            <label htmlFor="id">ID</label>
            <input readOnly type="text" id="id" name="id" size="1" value={job.id < 10 ? '0' + job.id : job.id}></input>

            <label htmlFor="company">Company</label>
            <input type="text" id="company" name="company" defaultValue={job.company}></input>

            <label htmlFor="title">Title</label>
            <input type="text" id="title" name="title" defaultValue={job.title}></input>

            <label htmlFor="applied">Applied</label>
            <input type="text" id="applied" name="applied" defaultValue={job.dateApplied}></input>
          </div>
        ))}
      </div>
    );
  }
}
export default Jobs;